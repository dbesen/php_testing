<?
	class testExecutableTest extends executableTest
	{
		function testIsEqual() {
			if($this->isEqual(1, 2)) $this->fail();
			if($this->isEqual(1, "1")) $this->fail();
			if($this->isEqual(1, true)) $this->fail();

			if(!$this->isEqual(1, 1)) $this->fail();
			if(!$this->isEqual("", "")) $this->fail();
			if(!$this->isEqual("a", "a")) $this->fail();
		}

		function testAssertEquals() {
			$this->assertEquals(1, 1);
			$this->assertEquals(1, 2);
			$count = $this->getFailureCount();
			$this->clearFailures();
			if($count !== 1) $this->fail("failure count expected 1, was " . $this->getFailureCount());
			$this->assertEquals(1, 1);
		}

		function testFailureCount() {
			$this->assertEquals(0, $this->getFailureCount());
			$this->assertEquals(0, $this->getFailureCount());
			$this->fail();
			$this->assertEquals(1, $this->getFailureCount());
			$this->assertEquals(1, $this->getFailureCount());
			$this->fail();
			$this->assertEquals(2, $this->getFailureCount());
			$this->assertEquals(2, $this->getFailureCount());
			if($this->getFailureCount() == 2) $this->clearFailures();
			$this->assertEquals(0, $this->getFailureCount());
			$this->assertEquals(0, $this->getFailureCount());
		}

		function testAssertTrue() {
			$this->assertTrue(true);

			$this->assertTrue(false);
			$this->expectFailure();

			$this->assertTrue(true);

			$this->assertTrue(1);
			$this->assertTrue("1");
			$this->expectFailure(2);
		}

		function testAssertFalse() {
			$this->assertFalse(false);
			$this->assertFalse(true);
			$this->expectFailure();
			$this->assertFalse(0);
			$this->expectFailure();
		}
	}
?>
