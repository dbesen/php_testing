<?
	class executableTest {
		private $failures = array();
		function assertEquals($expected, $actual) {
			if(!$this->isEqual($expected, $actual)) $this->fail();
		}

		function fail($msg = "") {
			$f = array();
			$f['msg'] = $msg;
			$f['backtrace'] = debug_backtrace();
			$this->failures[] = $f;
		}

		function isEqual($expected, $actual) {
			if($expected === $actual) return true;
			return false;
		}

		function run() {
			$method_names = get_class_methods($this);
			foreach($method_names as $method_name) {
				if(preg_match("/^test/i", $method_name)) {
					$this->$method_name();
				}
				$this->print_failures();
			}
		}
		function print_failures() {
			if($this->getFailureCount() == 0) return;
			print_r($this->failures);
		}
		function clearFailures() {
			$this->failures = array();
		}
		function getFailureCount() {
			return count($this->failures);
		}
		function assertTrue($arg) {
			$this->assertEquals($arg, true);
		}
		function assertFalse($arg) {
			$this->assertEquals($arg, false);
		}
		function expectFailure($count = 1) {
			if($this->getFailureCount() !== $count) $this->fail();
			else $this->clearFailures();
		}

	}
?>
