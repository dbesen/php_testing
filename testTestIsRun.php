<?
	require_once("testWasRun.php");
	class testTestIsRun extends executableTest {
		function testTestWasRun() {
			$test = new testWasRun();
			$this->assertEquals(0, $test->getRunCount());
			$test->run();
			$this->assertEquals(1, $test->getRunCount());
			$test->run();
			$this->assertEquals(2, $test->getRunCount());
		}
	}
?>
